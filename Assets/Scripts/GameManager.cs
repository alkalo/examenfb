﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private InputManager inputManager;
    private HUD hud;
    private Kirby kirby;
    private Spawner spawner;

    public bool isPaused;
    public bool gameover;
    public bool startGame = false;    
    private float timeCounter;
	public AudioSource menusong;
	public AudioSource gameplaysong;
	public AudioSource gameoversong;



	void Start ()
    {
        isPaused = false;
        startGame = false;

        inputManager = new InputManager();
        inputManager.Initialize();

        hud = GameObject.FindGameObjectWithTag("HUD").GetComponent<HUD>();
        hud.ClosePausePanel();
        hud.CloseGameoverPanel();
        hud.CloseGameplayPanel();
        hud.OpenTitlePanel();
		menusong.Play ();


        kirby = GameObject.FindGameObjectWithTag("Player").GetComponent<Kirby>();
        kirby.Initialize();
        spawner = GameObject.FindGameObjectWithTag("Spawner").GetComponent<Spawner>();
        spawner.Initialize();

        timeCounter = 0;
        hud.UpdateTimeText(timeCounter);

        
    }
    void Update()
    {
        if(!startGame) return;
        if(gameover)
        {
            kirby.DeadUpdate();
            return;
        }

        inputManager.ReadInput(); //Update Input

        kirby.MyUpdate(); //Update kirby
        spawner.MyUpdate(); //Update spawner

        timeCounter += Time.deltaTime;
        hud.UpdateTimeText(timeCounter);
    }
    public void PauseGame()
    {
        isPaused = !isPaused;

        if(isPaused)
        {
            Time.timeScale = 0;
            hud.OpenPausePanel();
        }
        else
        {
            Time.timeScale = 1;
            hud.ClosePausePanel();
        }
    }
    public void StartGame()
    {
        startGame = true;
        hud.CloseTitlePanel();
        hud.OpenGameplayPanel();
		menusong.Stop ();
		gameplaysong.Play ();
    }
    public void GameOver()
    {
        gameover = true;

        SaveGame();

        hud.UpdateGameOverPanel();
        hud.OpenGameoverPanel();
		menusong.Stop ();
		gameplaysong.Stop ();
		gameoversong.Play ();
    }
    void SaveGame()
    {
        float record = 0;

        if(PlayerPrefs.HasKey("Record"))
        {
            record = PlayerPrefs.GetFloat("Record");
            if(timeCounter > record) record = timeCounter;
        }
        else record = timeCounter;

        PlayerPrefs.SetFloat("Record", record);
        PlayerPrefs.SetFloat("Score", timeCounter);
    }
    public void LoadScene(int num)
    {
        SceneManager.LoadScene(num);
    }
}
